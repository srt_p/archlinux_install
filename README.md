# Description
this installation script for Archlinux reads the install.txt from the home-directory of the Arch-Iso, prints the Paragraphs and asks for action after each paragraph. For most of the paragraphs a default action is suggested.

# Usage
- boot Archlinux live
- install git
- clone directory
- make `arch_install.sh` executable and execute it
