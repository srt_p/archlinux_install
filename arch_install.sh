#!/bin/bash

cur_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

BOLD=$(tput bold)
NORMAL=$(tput sgr0)
RED=$(tput setaf 1)
GREEN=$(tput setaf 2)
BLUE=$(tput setaf 4)
YELLOW=$(tput setaf 3)

# two blue bold colons with following text also bold
COLCOL="${BOLD}${BLUE}::${NORMAL}${BOLD}"

function ask_select {
    local question="$1"
    shift
    local expect_arg=false
    local commands=()
    local count=0
    local sel_count=0
    echo "${COLCOL} $question"
    for var in "$@"; do
        count=$(( count + 1 ))
        if $expect_arg; then
            expect_arg=false
            commands+=( "$var" )
        else
            expect_arg=true
	    sel_count=$(( sel_count + 1 ))
            echo "   ${BOLD}${YELLOW}$sel_count)${NORMAL}${BOLD} $var${NORMAL}"
        fi
    done
    if $expect_arg; then
        echo "${RED}${BOLD}error: internal error - function 'ask_select' expected additional argument${NORMAL}"
        exit
    fi
    while : ; do
        echo -n "${COLCOL} Select "
        local input=
        read input
        echo -n "${NORMAL}"
        if [[ "$input" =~ ^[0-9]+$ ]] && [ "$input" -le "$sel_count" ] && [ "$input" -gt "0" ]; then
		eval "${commands[$(( input - 1 ))]}"
            return
        fi
    done
}

function ask {
    local question="$1"
    shift
    local expect_arg=false
    local select_strings=()
    local commands=()
    local default=
    local count=0
    local var=
    local i=
    for var in "$@"; do
        if $expect_arg; then
            expect_arg=false
            commands+=( "$var" )
        else
            expect_arg=true
            local first_char="${var::1}"
            if [ "$first_char" = "${first_char^^}" ]; then
                if [ -z $default ]; then
                    default=$count
                else
                    echo "${RED}${BOLD}error: internal error - function 'ask' can only have one default (uppercase) argument${NORMAL}"
                    exit
                fi
            fi
            select_strings+=( "$var" )
            count=$(( count + 1 ))
        fi
    done
    if $expect_arg; then
        echo "${RED}${BOLD}error: internal error - function 'ask' expected additional argument${NORMAL}"
        exit
    fi
    while : ; do
        echo -n $(IFS=/ eval 'echo "${COLCOL} $question [${select_strings[*]}]"')
        local input=
        read -p " " input
        echo -n ${NORMAL}
        for i in ${!select_strings[*]}; do
            if [ "${select_strings[$i]}" = "$input" ]; then
                eval "${commands[$i]}"
                return
            fi
        done
        if [ -n $default ] && [ -z $input ]; then
            eval "${commands[$default]}"
            return
        fi
    done
}

function execute_command {
    local inp=
    ask "Execute '${GREEN}$1${NORMAL}${BOLD}'${NORMAL}" \
        "Y" "eval \"$1\"" \
        "n" "exit" \
        "e" "read -e -i \"$1\" -p \"\${COLCOL} Execute: \${GREEN}\" inp; echo -n \${NORMAL}; eval \"\$inp\"" \
        "b" "bash"
}

function act {
    . "$cur_dir/arch_install_act.sh"
}

function main {
    echo "install pacman-contrib (for ranking the mirrors)"
    pacman -Sy --needed pacman-contrib
    
    local paragraphs_num=()
    local paragraphs_head=()
    while IFS= read -r line; do
        paragraphs_num+=( "${line%% *}" )
        paragraphs_head+=( "${line#* }" )
    done < <( sed -n -e '/^Contents$/,/^[^[:blank:]].*$/p' ~/install.txt \
        | sed -n -e '/^[[:blank:]]/p' \
        | sed -e 's/^[[:blank:]]*[^0-9]*[[:blank:]]*\([0-9.]*\)[[:blank:]]*\(.*\)$/\1 \2/g' )
    
    echo
    printf "${BLUE}${BOLD}Contents${NORMAL}\n"
    for i in ${!paragraphs_num[*]}; do
        echo " ${paragraphs_num[$i]} ${paragraphs_head[$i]}"
    done
    
    echo
    echo ${COLCOL} default option naming:${NORMAL}
    echo " y -> yes"
    echo " n -> no (exit)"
    echo " e -> edit command"
    echo " b -> open bash prompt"
    echo
    
    for i in ${!paragraphs_num[*]}; do
        ask "Continue" "Y" "" "n" "exit" "b" "bash"
        echo
        # the last pipes are for removing the second (an empty) line and for middle paragraphs also the last two lines (empty + next heading)
        local var=$( [[ $i = $((${#paragraphs_num[*]} - 1)) ]] \
            && sed -n -e "/^${paragraphs_head[$i]}$/,\$p" ~/install.txt | sed -n '1p;3,$p' \
            || sed -n -e "/^${paragraphs_head[$i]}$/,/^${paragraphs_head[$(($i + 1))]}$/p" ~/install.txt | head -n -2 | sed -n '1p;3,$p' )
        if [[ ! ${paragraphs_num[$i]} == *"."* ]]; then
            echo "${BLUE}${BOLD}==============================================================================${NORMAL}"
        fi
        echo "${BLUE}${BOLD}${paragraphs_head[$i]}${NORMAL}"
        if [[ ! ${paragraphs_num[$i]} == *"."* ]]; then
            echo "${BLUE}${BOLD}==============================================================================${NORMAL}"
        fi
        echo "$var" | tail -n +2
        echo
        act "${paragraphs_head[$i]}"
    done
}

main
