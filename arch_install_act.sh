keymap="de-latin1"
lang="en_GB.UTF-8"
timezone="Europe/Berlin"
root_dir="/mnt"
hostname=

# for boot loaders
grub_disk=/dev/sda
efi_part_mount_point=/boot
microcode_initrd=

function boot_loader_install {
    if [ "$1" = "bios" ]; then
        if [ "$2" = "grub" ]; then
            ask "Confirm grub disk, current: $grub_disk" "Y" "" "e" "read -e -i \"$grub_disk\" -p \"\${COLCOL} grub disk: \${GREEN}\" grub_disk; echo -n \${NORMAL}"
            execute_command "arch-chroot $root_dir pacman -S --needed grub"
            execute_command "arch-chroot $root_dir grub-install --target=i386-pc $grub_disk"
            execute_command "arch-chroot $root_dir sed -i -e 's/^GRUB_TIMEOUT=.*/GRUB_TIMEOUT=0/' /etc/default/grub"
            execute_command "arch-chroot $root_dir grub-mkconfig -o /boot/grub/grub.cfg"
        else
            echo "${RED}${BOLD}error: internal error - no entry for bios boot loader '$2'${NORMAL}"
        fi
    elif [ "$1" = "uefi" ]; then
        if [ "$2" = "grub" ]; then
            ask "Confirm efi partition mount point (chrooted), current: $efi_part_mount_point" "Y" "" "e" "read -e -i \"$efi_part_mount_point\" -p \"\${COLCOL} mount point: \${GREEN}\" efi_part_mount_point; echo -n \${NORMAL}"
            execute_command "arch-chroot $root_dir pacman -S --needed grub efibootmgr"
            execute_command "arch-chroot $root_dir grub-install --target=x86_64-efi --efi-directory=$efi_part_mount_point --bootloader-id=GRUB"
            execute_command "arch-chroot $root_dir sed -i -e 's/^GRUB_TIMEOUT=.*/GRUB_TIMEOUT=0/' /etc/default/grub"
            execute_command "arch-chroot $root_dir grub-mkconfig -o /boot/grub/grub.cfg"
        elif [ "$2" = "systemd-boot" ]; then
            execute_command "bootctl --path=/boot install"
            ask "Configure automatic update" "Y" "printf '[Trigger]\\nType = Package\\nOperation = Upgrade\\nTarget = systemd\\n\\n[Action]\\nDescription = Updating systemd-boot\\nWhen = PostTransaction\\nExec = /usr/bin/bootctl update' > $root_dir/etc/pacman.d/hooks/100-systemd-boot.hook" "n" "" "b" "bash"
            execute_command "printf 'default arch\\ntimeout 0\\nconsole-mode max\\neditor no' > $root_dir/boot/loader/loader.conf"
            echo "${RED}${BOLD}TODO check this${NORMAL}"
            local root_dev=$(df -P $root_dir | awk 'END{print \$1}')
            local root_uuid=$(lsblk -no UUID $root_dev)
            if [ -n $microcode_initrd ]; then
                execute_command "printf 'title Arch Linux\\nlinux /vmlinuz-linux\\ninitrd /$microcode_initrd\\ninitrd /initramfs-linux.img\\noptions root=UUID=$root_uuid rw' > $root_dir/boot/loader/entries/arch.conf"
            else
                execute_command "printf 'title Arch Linux\\nlinux /vmlinuz-linux\\ninitrd /initramfs-linux.img\\noptions root=UUID=$root_uuid rw' > $root_dir/boot/loader/entries/arch.conf"
            fi
        else
            echo "${RED}${BOLD}error: internal error - no entry for uefi boot loader '$2'${NORMAL}"
        fi
    else
        echo "${RED}${BOLD}error: internal error - no entry for '$1' boot loader${NORMAL}"
    fi
}

if [ "$1" = "Set the keyboard layout" ]; then
    ask "Confirm keymap, current: $keymap" "Y" "" "e" "read -e -i \"$keymap\" -p \"\${COLCOL} keymap: \${GREEN}\" keymap; echo -n \${NORMAL}"
    execute_command "loadkeys $keymap"
elif [ "$1" = "Verify the boot mode" ]; then
    if [ -d /sys/firmware/efi/efivars ]; then
        echo "${COLCOL} You are in UEFI mode${NORMAL}"
    else
        echo "${COLCOL} You are in BIOS mode${NORMAL}"
    fi
elif [ "$1" = "Connect to the internet" ]; then
    execute_command "ping -c 1 www.archlinux.org"
elif [ "$1" = "Update the system clock" ]; then
    execute_command "timedatectl set-ntp true"
elif [ "$1" = "Example layouts" ]; then
    execute_command "bash"
elif [ "$1" = "Format the partitions" ]; then
    execute_command "bash"
elif [ "$1" = "Mount the file systems" ]; then
    execute_command "bash"
elif [ "$1" = "Select the mirrors" ]; then
    execute_command "curl -s 'https://www.archlinux.org/mirrorlist/?protocol=https&use_mirror_status=on' | sed -e 's/^#Server/Server/' -e '/^#/d' | rankmirrors - > /etc/pacman.d/mirrorlist"
elif [ "$1" = "Install essential packages" ]; then
    ask "Confirm root directory (without spaces), current: $root_dir" "Y" "" "e" "read -e -i \"$root_dir\" -p \"\${COLCOL} root dir: \${GREEN}\" root_dir; echo -n \${NORMAL}"
    execute_command "pacstrap $root_dir base linux linux-firmware"
elif [ "$1" = "Fstab" ]; then
    execute_command "genfstab -U $root_dir >> /mnt/etc/fstab"
elif [ "$1" = "Time zone" ]; then
    ask "Confirm timezone, current: $timezone" "Y" "" "e" "read -e -i \"$timezone\" -p \"\${COLCOL} timezone: \${GREEN}\" timezone; echo -n \${NORMAL}"
    execute_command "arch-chroot $root_dir ln -sf /usr/share/zoneinfo/$timezone /etc/localtime"
    execute_command "arch-chroot $root_dir hwclock --systohc"
elif [ "$1" = "Localization" ]; then
    ask "Confirm language, current: $lang" "Y" "" "e" "read -e -i \"$lang\" -p \"\${COLCOL} language: \${GREEN}\" lang; echo -n \${NORMAL}"
    execute_command "arch-chroot $root_dir sed -i -e 's/^#$lang/$lang/' /etc/locale.gen"
    execute_command "arch-chroot $root_dir locale-gen"
    execute_command "echo LANG=$lang > $root_dir/etc/locale.conf"
    execute_command "echo KEYMAP=$keymap > $root_dir/etc/vconsole.conf"
elif [ "$1" = "Network configuration" ]; then
    while : ; do
        read -e -p "${COLCOL} hostname: ${GREEN}" hostname
        echo -n ${NORMAL}
        if [ -n "$hostname" ]; then break; fi
    done
    execute_command "echo $hostname > $root_dir/etc/hostname"
    execute_command "printf '127.0.0.1 localhost\\n::1 localhost\\n127.0.1.1 $hostname.localdomain $hostname' > $root_dir/etc/hosts"
elif [ "$1" = "Root password" ]; then
    execute_command "while : ; do arch-chroot $root_dir passwd && break; done"
elif [ "$1" = "Boot loader" ]; then
    echo "${COLCOL} Installing microcode updates${NORMAL}"
    local cpu_vendor=$(grep -i -m 1 vendor_id /proc/cpuinfo | tr '[:upper:]' '[:lower:]')
    if [[ "$cpu_vendor" =~ "intel" ]]; then
        execute_command "arch-chroot $root_dir pacman -S --needed intel-ucode"
        microcode_initrd="intel-ucode.img"
    elif [[ "$cpu_vendor" =~ "amd" ]]; then
        execute_command "arch-chroot $root_dir pacman -S --needed amd-ucode"
        microcode_initrd="amd-ucode.img"
    else
        echo "${COLCOL} no fitting microcode update found, continuing${NORMAL}"
    fi
    if [ -d /sys/firmware/efi/efivars ]; then
        # uefi boot loaders
        ask_select "Select uefi boot loader" grub "boot_loader_install uefi grub" systemd-boot "boot_loader_install uefi systemd-boot" "(manual configuration, enter bash shell)" "bash"
    else
        # bios boot loaders
        ask_select "Select bios boot loader" grub "boot_loader_install bios grub" "(manual configuration, enter bash shell)" "bash"
    fi
elif [ "$1" = "Reboot" ]; then
    execute_command "umount -R $root_dir"
    execute_command "reboot"
fi

